﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using azuredeploy.Models;
using Microsoft.EntityFrameworkCore;

namespace azuredeploy.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PessoasController : ControllerBase
    {
        private readonly ILogger<PessoasController> _logger;
        private readonly BancoDeDadosContext _context;

        public PessoasController(ILogger<PessoasController> logger, BancoDeDadosContext context)
        {
            _logger = logger;
            _context = context;
        }

        //GET: /Pessoas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Pessoa>>> GetPessoas()
        {
            return await _context.Pessoas.AsNoTracking().ToListAsync();
        }

    }
}
