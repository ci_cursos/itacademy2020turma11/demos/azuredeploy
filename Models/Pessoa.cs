﻿using System;
using System.Collections.Generic;

#nullable disable

namespace azuredeploy.Models
{
    public partial class Pessoa
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public int? Idade { get; set; }
    }
}
